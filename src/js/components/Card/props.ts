export default interface CardPropsInterface {
	icon?: string;
	background?: boolean;
	title: string;
	children: any;
}